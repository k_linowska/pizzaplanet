﻿using AutoMapper;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaPlanet.Business.Services
{
    public interface IPizzasService
    {
        void Add(PizzaBl pizza);
        List<PizzaBl> GetAllPizzas();
        bool IsPizzasNameTaken(PizzaBl pizza);
        List<PizzaBl> ShowMenu();
    }

    public class PizzasService : IPizzasService
    {
        private readonly Func<IPizzaPlanetContext> _context;

        public PizzasService(Func<IPizzaPlanetContext> context)
        {
            _context = context;
        }

        public bool IsPizzasNameTaken(PizzaBl pizza)
        {
            using (var ctx = _context().GetContext())
            {
                return ctx.Pizzas.Any(i => i.Name == pizza.Name);
            }
        }

        public List<PizzaBl> ShowMenu()
        {
            List<PizzaBl> pizzas = GetAllPizzas();

            using (var ctx = _context().GetContext())
            {
                if (ctx.Pizzas.Any())
                {
                    return pizzas;
                }
            }

            Console.WriteLine("The list of pizzas is empty.");
            return pizzas;
        }

        public List<PizzaBl> GetAllPizzas()
        {
            using (var ctx = _context().GetContext())
            {
                return ctx.Pizzas.Include("Ingredients")
                    .Include("Sauce")
                    .ToList()
                    .Select(p => Mapper.Map<Business.Models.PizzaBl>(p))
                    .ToList();
            }
        }

        public void Add(PizzaBl pizza)
        {
            using (var ctx = _context().GetContext())
            {
                var pizzaToAdd = new Pizza
                {
                    Name = pizza.Name,
                    PriceForSizeS = pizza.PriceForSizeS,
                    PriceForSizeM = pizza.PriceForSizeM,
                    PriceForSizeL = pizza.PriceForSizeL,
                    SauceId = pizza.SauceId,
                };

                foreach (var item in pizza.IngredientsId)
                {
                    pizzaToAdd.Ingredients.Add(ctx.Ingredients.Find(item));
                }

                ctx.Pizzas.Add(pizzaToAdd);
                ctx.SaveChanges();
            }
        }
    }
}