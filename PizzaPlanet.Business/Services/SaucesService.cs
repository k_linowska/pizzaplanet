﻿using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace PizzaPlanet.Business.Services
{
    public interface ISaucesService
    {
        void Add(SauceBl sauce);
        SauceBl FindSauce(int sauceId);
        List<SauceBl> GetAllSauces();
    }

    public class SaucesService : ISaucesService
    {
        private readonly Func<IPizzaPlanetContext> _context;

        public SaucesService(Func<IPizzaPlanetContext> context)
        {
            _context = context;
        }

        public void Add(SauceBl sauce)
        {
            var sauceData = Mapper.Map<Data.Models.Sauce>(sauce);

            using (var ctx = _context().GetContext())
            {
                if (!ctx.Sauces.Any(i => i.Name == sauce.Name))
                {
                    ctx.Sauces.Add(sauceData);
                    ctx.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Sauce already added");
                }
            }
        }

        public List<SauceBl> GetAllSauces()
        {
            using (var ctx = _context().GetContext())
            {
                return ctx.Sauces.ToList().Select(s => Mapper.Map<Business.Models.SauceBl>(s)).ToList();
            }
        }

        public SauceBl FindSauce(int sauceId)
        {
            using (var ctx = _context().GetContext())
            {
                return Mapper.Map<Business.Models.SauceBl>(ctx.Sauces.FirstOrDefault(i => i.Id == sauceId));
            }
        }
    }
}