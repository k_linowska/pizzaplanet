﻿using AutoMapper;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Models;
using System;
using System.ComponentModel;

namespace PizzaPlanet.Business.Services
{
    public interface IOrdersService
    {
        void AddOrder(OrderBl order);
        PizzaToOrderBl GetPizzaToOrder(PizzaBl pizza, PizzaToOrderBl.Size size);
        decimal GetPrice(PizzaToOrderBl.Size pizzaSize, PizzaBl pizzaBl);
        decimal CheckDiscount(PizzaToOrderBl.Size pizzaSize, PizzaBl pizzaBl, decimal price);
        void AddPizzaToOrder(PizzaToOrderBl ordered);
    }

    public class OrdersService : IOrdersService
    {
        private IDateTimeProvider _dateTimeProvider;
        private readonly Func<IPizzaPlanetContext> _context;

        public OrdersService(IDateTimeProvider dateTimeProvider, Func<IPizzaPlanetContext> context)
        {
            _dateTimeProvider = dateTimeProvider;
            _context = context;
        }

        public PizzaToOrderBl GetPizzaToOrder(PizzaBl pizza, PizzaToOrderBl.Size size)
        {
            return new PizzaToOrderBl
            {
                Pizza = pizza,
                PizzaSize = size,
                Price = GetPrice(size, pizza),
                PizzaId = pizza.Id
            };
        }       

        public decimal GetPrice(PizzaToOrderBl.Size pizzaSize, PizzaBl pizzaBl)
        {
            decimal price = 0m;

            switch (pizzaSize)
            {
                case PizzaToOrderBl.Size.S:
                    price = pizzaBl.PriceForSizeS;
                    break;
                case PizzaToOrderBl.Size.M:
                    price = pizzaBl.PriceForSizeM;
                    break;
                case PizzaToOrderBl.Size.L:
                    price = pizzaBl.PriceForSizeL;
                    break;
                default:
                    throw new InvalidEnumArgumentException("Invalid pizza size enumeration value");
            }

            price = CheckDiscount(pizzaSize, pizzaBl, price);
            return price;
        }

        public decimal CheckDiscount(PizzaToOrderBl.Size pizzaSize, PizzaBl pizzaBl, decimal price)
        {
            var dayOfWeek = _dateTimeProvider.GetDayOfWeek();

            if (dayOfWeek == DayOfWeek.Wednesday && pizzaSize == PizzaToOrderBl.Size.L)
            {
                return pizzaBl.PriceForSizeM;
            }

            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                return price / 2;
            }

            return price;
        }

        public void AddOrder(OrderBl order)
        {
            using (var ctx = _context().GetContext())
            {
                var orderToAdd = new Order
                {
                    ClientId = order.ClientId,
                    Cost = order.Cost,

                };

                foreach (var item in order.PizzasToOrderId)
                {
                    orderToAdd.PizzasToOrder.Add(ctx.PizzasToOrder.Find(item));
                }

                foreach (var item in order.ExtraSaucesId)
                {
                    orderToAdd.ExtraSauces.Add(ctx.Sauces.Find(item));
                }

                ctx.Orders.Add(orderToAdd);
                ctx.SaveChanges();
            }
        }

        public void AddPizzaToOrder(PizzaToOrderBl ordered)
        {
            var pizza = Mapper.Map<PizzaToOrder>(ordered);

            using (var ctx = _context().GetContext())
            {
                pizza.Pizza = null;
                pizza.PizzaId = ordered.PizzaId;

                foreach (var item in ordered.FreeSaucesId)
                {
                    pizza.FreeSauces.Add(ctx.Sauces.Find(item));
                }

                ctx.PizzasToOrder.Add(pizza);
                ctx.SaveChanges();
            }

            ordered.Id = pizza.Id;
        }
    }
}
