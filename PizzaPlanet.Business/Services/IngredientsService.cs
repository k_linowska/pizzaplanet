﻿using System;
using System.Collections.Generic;
using System.Linq;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using AutoMapper;
using PizzaPlanet.Data.Models;

namespace PizzaPlanet.Business.Services
{
    public interface IIngredientsService
    {
        void Add(IngredientBl ingredient);
        IngredientBl FindIngredient(int ingredientId);
        List<IngredientBl> GetAllIngredients();
    }

    public class IngredientsService : IIngredientsService
    {
        private readonly Func<IPizzaPlanetContext> _context;

        public IngredientsService(Func<IPizzaPlanetContext> context)
        {
            _context = context;
        }

        public void Add(IngredientBl ingredient)
        {
            var ingredientToAdd = Mapper.Map<Ingredient>(ingredient);

            using (var ctx = _context().GetContext())
            {
                if (!ctx.Ingredients.Any(i => i.Name == ingredient.Name))
                {
                    ctx.Ingredients.Add(ingredientToAdd);
                    ctx.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Ingredient already added");
                }
            }

        }

        public List<IngredientBl> GetAllIngredients()
        {
            using (var ctx = _context().GetContext())
            {
                return ctx.Ingredients.ToList().Select(i => Mapper.Map<IngredientBl>(i)).ToList();
            }
        }

        public IngredientBl FindIngredient(int ingredientId)
        {
            using (var ctx = _context().GetContext())
            {
                return Mapper.Map<IngredientBl>(ctx.Ingredients.FirstOrDefault(i => i.Id == ingredientId));
            }
        }
    }
}