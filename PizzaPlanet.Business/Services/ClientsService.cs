﻿using AutoMapper;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Models;
using System;
using System.Linq;

namespace PizzaPlanet.Business.Services
{
    public interface IClientsService
    {
        void AddClient(ClientBl client);
        ClientBl FindClient(string phoneNumber);
        void UpdateClient(int id, DateTime now);
    }

    public class ClientsService : IClientsService
    {
        private readonly Func<IPizzaPlanetContext> _context;

        public ClientsService(Func<IPizzaPlanetContext> context)
        {
            _context = context;
        }

        public void AddClient(ClientBl client)
        {
            Client clientToAdd;

            using (var ctx = _context().GetContext())
            {
                clientToAdd = ctx.Clients.Add(Mapper.Map<Client>(client));
                ctx.SaveChanges();
            }

            client.Id = clientToAdd.Id;
        }

        public ClientBl FindClient(string phoneNumber)
        {
            using (var ctx = _context().GetContext())
            {
                return Mapper.Map<ClientBl>(ctx.Clients.SingleOrDefault(c => c.PhoneNumber == phoneNumber));
            }
        }

        public void UpdateClient(int id, DateTime now)
        {
            using (var ctx = _context().GetContext())
            {
                ctx.Clients.SingleOrDefault(c => c.Id == id).LastOrderDate = now;
                ctx.SaveChanges();
            }
        }
    }
}
