﻿using PizzaPlanet.Data;
using System;
using System.Linq;

namespace PizzaPlanet.Business.Services
{
    public interface IDatabaseService
    {
        bool CheckIfDatabaseNotEmpty();
        bool CheckIfPizzasNotEmpty();
    }

    public class DatabaseService : IDatabaseService
    {
        private readonly Func<IPizzaPlanetContext> _context;

        public DatabaseService(Func<IPizzaPlanetContext> context)
        {
            _context = context;
        }        

        public bool CheckIfDatabaseNotEmpty()
        {
            using (var ctx = _context().GetContext())
            {
                if (ctx.Ingredients.Any() && ctx.Sauces.Any())
                {
                    return true;
                }
            }
            return false;
        }

        public bool CheckIfPizzasNotEmpty()
        {
            using (var ctx = _context().GetContext())
            {
                if (ctx.Pizzas.Any())
                {
                    return true;
                }
            }
            return false;
        }
    }
}