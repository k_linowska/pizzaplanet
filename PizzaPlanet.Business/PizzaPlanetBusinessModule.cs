﻿using Ninject;
using Ninject.Modules;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Services;
using PizzaPlanet.Data;

namespace PizzaPlanet.Business
{
    public class PizzaPlanetBusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IIngredientsService>().To<IngredientsService>();
            Kernel.Bind<IOrdersService>().To<OrdersService>();
            Kernel.Bind<IDatabaseService>().To<DatabaseService>();
            Kernel.Bind<IPizzasService>().To<PizzasService>();
            Kernel.Bind<ISaucesService>().To<SaucesService>();
            Kernel.Bind<IClientsService>().To<ClientsService>();
            Kernel.Bind<IDateTimeProvider>().To<DateTimeProvider>();
            Kernel.Load(new PizzaPlanetDataModule());
        }
    }
}
