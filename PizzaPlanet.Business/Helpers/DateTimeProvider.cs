﻿using System;

namespace PizzaPlanet.Business.Helpers
{
    public interface IDateTimeProvider
    {
        DayOfWeek GetDayOfWeek();
    }

    public class DateTimeProvider : IDateTimeProvider
    {
        public DayOfWeek GetDayOfWeek()
        {
            return DateTime.Now.DayOfWeek;
        }
    }
}
