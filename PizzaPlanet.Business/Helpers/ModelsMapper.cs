﻿using AutoMapper;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data.Models;

namespace PizzaPlanet.Business.Helpers
{
    public class ModelsMapper
    {
        public static void AutoMapperInitialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Ingredient, IngredientBl>();
                cfg.CreateMap<IngredientBl, Ingredient>();
                cfg.CreateMap<Sauce, SauceBl>();
                cfg.CreateMap<SauceBl, Sauce>();
                cfg.CreateMap<Pizza, PizzaBl>();
                cfg.CreateMap<PizzaBl, Pizza>();
                cfg.CreateMap<OrderBl, Order>();
                cfg.CreateMap<PizzaToOrderBl, PizzaToOrder>();
                cfg.CreateMap<Client, ClientBl>();
                cfg.CreateMap<ClientBl, Client>();
            });
        }   
    }
}
