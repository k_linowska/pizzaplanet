﻿namespace PizzaPlanet.Business.Models
{
    public class IngredientBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}