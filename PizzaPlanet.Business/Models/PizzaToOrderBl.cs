﻿using System.Collections.Generic;

namespace PizzaPlanet.Business.Models
{
    public class PizzaToOrderBl
    {
        public enum Size
        {
            S = 1,
            M,
            L
        }

        public PizzaToOrderBl()
        {
            FreeSaucesId = new List<int>();
        }

        public int Id { get; set; }
        public decimal Price { get; set; }
        public Size PizzaSize { get; set; }
        public List<int> FreeSaucesId { get; set; }
        public int PizzaId { get; set; }
        public PizzaBl Pizza { get; set; }        
    }
}
