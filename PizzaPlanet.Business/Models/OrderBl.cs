﻿using System.Collections.Generic;

namespace PizzaPlanet.Business.Models
{
    public class OrderBl
    {
        public OrderBl()
        {
            PizzasToOrderId = new List<int>();
            ExtraSaucesId = new List<int>();
        }

        public int Id { get; set; }
        public decimal Cost { get; set; }
        public int ClientId { get; set; }        
        public List<int> ExtraSaucesId { get; set; }
        public List<int> PizzasToOrderId { get; set; }
    }
}
