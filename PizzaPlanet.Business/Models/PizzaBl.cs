﻿using System.Collections.Generic;

namespace PizzaPlanet.Business.Models
{
    public class PizzaBl
    {
        public PizzaBl()
        {
            IngredientsId = new List<int>();
            Ingredients = new List<IngredientBl>();
            Sauce = new SauceBl();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public List<IngredientBl> Ingredients { get; set; }
        public List<int> IngredientsId { get; set; }
        public SauceBl Sauce { get; set; }
        public int SauceId { get; set; }
        public decimal PriceForSizeS { get; set; }
        public decimal PriceForSizeM { get; set; }
        public decimal PriceForSizeL { get; set; }
    }
}