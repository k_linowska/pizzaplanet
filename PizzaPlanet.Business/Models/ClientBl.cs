﻿using System;

namespace PizzaPlanet.Business.Models
{
    public class ClientBl
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public DateTime LastOrderDate { get; set; }
    }
}
