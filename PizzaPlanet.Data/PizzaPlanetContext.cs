﻿using PizzaPlanet.Data.Models;
using System;
using System.Data.Entity;

namespace PizzaPlanet.Data
{
    public interface IPizzaPlanetContext : IDisposable
    {
        PizzaPlanetContext GetContext();
        DbSet<Client> Clients { get; set; }
        DbSet<Ingredient> Ingredients { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Pizza> Pizzas { get; set; }
        DbSet<PizzaToOrder> PizzasToOrder { get; set; }
        DbSet<Sauce> Sauces { get; set; }
    }

    public class PizzaPlanetContext : DbContext, IPizzaPlanetContext
    {
        public PizzaPlanetContext() : base("PizzaPlanetDbConnectionString")
        { }

        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Sauce> Sauces { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<PizzaToOrder> PizzasToOrder { get; set; }
        public DbSet<Client> Clients { get; set; }

        public PizzaPlanetContext GetContext()
        {
            return new PizzaPlanetContext();
        }
    }
}
