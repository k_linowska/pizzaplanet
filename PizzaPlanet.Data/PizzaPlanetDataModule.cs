﻿using Ninject.Modules;

namespace PizzaPlanet.Data
{
    public class PizzaPlanetDataModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IPizzaPlanetContext>().ToMethod(x => new PizzaPlanetContext());
        }
    }
}
