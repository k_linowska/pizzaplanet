﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Ingredient
    {
        public Ingredient()
        {
            Pizzas = new List<Pizza>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Pizza> Pizzas { get; set; }
    }
}
