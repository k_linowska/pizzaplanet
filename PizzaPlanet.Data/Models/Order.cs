﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Order
    {
        public Order()
        {
            PizzasToOrder = new List<PizzaToOrder>();
            ExtraSauces = new List<Sauce>();
        }

        public int Id { get; set; }
        public decimal Cost { get; set; }
        public int ClientId { get; set; }
        public ICollection<PizzaToOrder> PizzasToOrder { get; set; }
        public ICollection<Sauce> ExtraSauces { get; set; }
    }
}
