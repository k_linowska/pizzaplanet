﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Pizza
    {
        public Pizza()
        {
            Ingredients = new List<Ingredient>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal PriceForSizeS { get; set; }
        public decimal PriceForSizeM { get; set; }
        public decimal PriceForSizeL { get; set; }
        public int SauceId { get; set; }
        public Sauce Sauce { get; set; }
        public ICollection<Ingredient> Ingredients { get; set; }
    }
}
