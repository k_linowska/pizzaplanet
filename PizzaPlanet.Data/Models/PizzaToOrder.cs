﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class PizzaToOrder
    {
        public enum Size
        {
            S = 1,
            M,
            L
        }

        public PizzaToOrder()
        {
            FreeSauces = new List<Sauce>();
        }

        public int Id { get; set; }
        public decimal Price { get; set; }
        public Size PizzaSize { get; set; }
        public ICollection<Sauce> FreeSauces { get; set; }
        public int PizzaId { get; set; }
        public Pizza Pizza { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
