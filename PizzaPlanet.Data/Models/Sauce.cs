﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Sauce
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public ICollection<Order> ExtraSaucesToOrder { get; set; }
        public ICollection<PizzaToOrder> FreeSaucesForPizza { get; set; }
    }
}
