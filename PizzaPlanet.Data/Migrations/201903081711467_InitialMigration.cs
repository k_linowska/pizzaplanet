namespace PizzaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        Address = c.String(),
                        LastOrderDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pizzas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PriceForSizeS = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceForSizeM = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceForSizeL = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SauceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sauces", t => t.SauceId, cascadeDelete: true)
                .Index(t => t.SauceId);
            
            CreateTable(
                "dbo.Sauces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PizzaToOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PizzaSize = c.Int(nullable: false),
                        PizzaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pizzas", t => t.PizzaId, cascadeDelete: true)
                .Index(t => t.PizzaId);
            
            CreateTable(
                "dbo.PizzaIngredients",
                c => new
                    {
                        Pizza_Id = c.Int(nullable: false),
                        Ingredient_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Pizza_Id, t.Ingredient_Id })
                .ForeignKey("dbo.Pizzas", t => t.Pizza_Id, cascadeDelete: true)
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id, cascadeDelete: true)
                .Index(t => t.Pizza_Id)
                .Index(t => t.Ingredient_Id);
            
            CreateTable(
                "dbo.OrderSauces",
                c => new
                    {
                        Order_Id = c.Int(nullable: false),
                        Sauce_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_Id, t.Sauce_Id })
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                .ForeignKey("dbo.Sauces", t => t.Sauce_Id, cascadeDelete: true)
                .Index(t => t.Order_Id)
                .Index(t => t.Sauce_Id);
            
            CreateTable(
                "dbo.PizzaToOrderSauces",
                c => new
                    {
                        PizzaToOrder_Id = c.Int(nullable: false),
                        Sauce_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PizzaToOrder_Id, t.Sauce_Id })
                .ForeignKey("dbo.PizzaToOrders", t => t.PizzaToOrder_Id, cascadeDelete: true)
                .ForeignKey("dbo.Sauces", t => t.Sauce_Id, cascadeDelete: false)
                .Index(t => t.PizzaToOrder_Id)
                .Index(t => t.Sauce_Id);
            
            CreateTable(
                "dbo.PizzaToOrderOrders",
                c => new
                    {
                        PizzaToOrder_Id = c.Int(nullable: false),
                        Order_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PizzaToOrder_Id, t.Order_Id })
                .ForeignKey("dbo.PizzaToOrders", t => t.PizzaToOrder_Id, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                .Index(t => t.PizzaToOrder_Id)
                .Index(t => t.Order_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pizzas", "SauceId", "dbo.Sauces");
            DropForeignKey("dbo.PizzaToOrders", "PizzaId", "dbo.Pizzas");
            DropForeignKey("dbo.PizzaToOrderOrders", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.PizzaToOrderOrders", "PizzaToOrder_Id", "dbo.PizzaToOrders");
            DropForeignKey("dbo.PizzaToOrderSauces", "Sauce_Id", "dbo.Sauces");
            DropForeignKey("dbo.PizzaToOrderSauces", "PizzaToOrder_Id", "dbo.PizzaToOrders");
            DropForeignKey("dbo.OrderSauces", "Sauce_Id", "dbo.Sauces");
            DropForeignKey("dbo.OrderSauces", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.PizzaIngredients", "Ingredient_Id", "dbo.Ingredients");
            DropForeignKey("dbo.PizzaIngredients", "Pizza_Id", "dbo.Pizzas");
            DropIndex("dbo.PizzaToOrderOrders", new[] { "Order_Id" });
            DropIndex("dbo.PizzaToOrderOrders", new[] { "PizzaToOrder_Id" });
            DropIndex("dbo.PizzaToOrderSauces", new[] { "Sauce_Id" });
            DropIndex("dbo.PizzaToOrderSauces", new[] { "PizzaToOrder_Id" });
            DropIndex("dbo.OrderSauces", new[] { "Sauce_Id" });
            DropIndex("dbo.OrderSauces", new[] { "Order_Id" });
            DropIndex("dbo.PizzaIngredients", new[] { "Ingredient_Id" });
            DropIndex("dbo.PizzaIngredients", new[] { "Pizza_Id" });
            DropIndex("dbo.PizzaToOrders", new[] { "PizzaId" });
            DropIndex("dbo.Pizzas", new[] { "SauceId" });
            DropTable("dbo.PizzaToOrderOrders");
            DropTable("dbo.PizzaToOrderSauces");
            DropTable("dbo.OrderSauces");
            DropTable("dbo.PizzaIngredients");
            DropTable("dbo.PizzaToOrders");
            DropTable("dbo.Orders");
            DropTable("dbo.Sauces");
            DropTable("dbo.Pizzas");
            DropTable("dbo.Ingredients");
            DropTable("dbo.Clients");
        }
    }
}
