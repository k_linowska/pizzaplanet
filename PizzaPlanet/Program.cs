﻿using PizzaPlanet.Cli.Helpers;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using PizzaPlanet.Business.Helpers;
using Ninject;
using PizzaPlanet.Business;

namespace PizzaPlanet.Cli
{
    internal class Program
    {
        private IDatabaseService _databaseService;
        private IOrdersService _ordersService;
        private IIngredientsService _ingredientsService;
        private ISaucesService _saucesService;
        private IPizzasService _pizzasService;
        private IClientsService _clientsService;
        private IMenu _menu;
        private IIoHelper _ioHelper;

        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new PizzaPlanetBusinessModule(), new PizzaPlanetCliModule());
            var program = kernel.Get<Program>();
            ModelsMapper.AutoMapperInitialize();
            Console.WriteLine("Welcome to Pizza Planet!");
            program.SetCommands();
            program.ProgramLoop();
        }

        public Program(
            IIngredientsService ingredientsService,
            IOrdersService ordersService,
            IDatabaseService databaseService, 
            IPizzasService pizzasService, 
            ISaucesService saucesService,
            IClientsService clientsService, 
            IMenu menu, 
            IIoHelper ioHelper)
        {
            _ingredientsService = ingredientsService;
            _ordersService = ordersService;
            _databaseService = databaseService;
            _pizzasService = pizzasService;
            _saucesService = saucesService;
            _clientsService = clientsService;
            _menu = menu;
            _ioHelper = ioHelper;
        }

        private void SetCommands()
        {
            _menu.SetCommand(AddIngredient, "Add Ingredient");
            _menu.SetCommand(AddSauce, "Add Sauce");
            _menu.SetCommand(AddPizza, "Add Pizza");
            _menu.SetCommand(ShowMenu, "Show Menu");
            _menu.SetCommand(Order, "Order");
            _menu.SetCommand(Exit, "Exit");
        }

        public void ProgramLoop()
        {
            do
            {
                _menu.PrintAllCommands();
                _menu.Execute(Console.ReadLine());
            }
            while (true);
        }

        private void AddIngredient()
        {
            string name = _ioHelper.GetUserInputStr("Give the ingredient's name: ");
            var ingredient = new IngredientBl() { Name = name };
            _ingredientsService.Add(ingredient);
        }

        private void AddSauce()
        {
            string name = _ioHelper.GetUserInputStr("Give the sauce's name: ");
            decimal price = _ioHelper.GetUserInputDecimal("Give the sauce's price: ");
            var sauce = new SauceBl() { Name = name, Price = price };
            _saucesService.Add(sauce);
        }

        private void AddPizza()
        {
            if (_databaseService.CheckIfDatabaseNotEmpty())
            {
                string name = _ioHelper.GetUserInputStr("Give the pizza's name: ");
                PizzaBl pizza = new PizzaBl { Name = name };
                
                if (!_pizzasService.IsPizzasNameTaken(pizza))
                {
                    SetPizzasPrices(pizza);
                    pizza.IngredientsId = AddIngredientsToPizza();
                    AddSauceToPizza(pizza);
                    _pizzasService.Add(pizza);
                    Console.WriteLine($"Added pizza: ");
                    _ioHelper.PrintPizzaInfo(pizza);
                }
                else
                {
                    Console.WriteLine($"Name {name} already taken");
                }
            }
            else { Console.WriteLine("The list of ingredients or sauces is empty."); }
        }

        private void SetPizzasPrices(PizzaBl pizza)
        {
            while (pizza.PriceForSizeS == 0)
            {
                pizza.PriceForSizeS = _ioHelper.GetUserInputDecimal("Give price for small size: ");
            }
            while (pizza.PriceForSizeM <= pizza.PriceForSizeS)
            {
                pizza.PriceForSizeM = _ioHelper.GetUserInputDecimal("Give price for medium size: ");
            }
            while (pizza.PriceForSizeL <= pizza.PriceForSizeM)
            {
                pizza.PriceForSizeL = _ioHelper.GetUserInputDecimal("Give price for large size: ");
            }
        }

        private List<int> AddIngredientsToPizza()
        {
            var ingredients = new List<IngredientBl>();
            var ingredientsId = new List<int>();
            const string requirements = "You need to add at least 1 ingredient and not more than 7. " +
                                        "One ingredient can be added max 2 times.";
            while (ingredients.Count < 7)
            {
                if (ingredients.Count == 0)
                {
                    Console.WriteLine(requirements);
                }

                try
                {
                    IngredientBl ingredient = GetIngredient("Available ingredients: ", _ingredientsService.GetAllIngredients());
                    if (ingredients.FindAll(i => i.Name == ingredient.Name).Count < 2)
                    {
                        ingredients.Add(ingredient);
                        ingredientsId.Add(ingredient.Id);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(requirements);
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine($"Added ingredients: {ingredients.Count()}");

                if (ingredients.Count == 7)
                {
                    break;
                }

                if (_ioHelper.GetUserInputStr("Do you want to add more ingredients? (y/n)").ToLower() == "n"
                    && ingredients.Count != 0)
                {
                    break;
                }
            }
            return ingredientsId;
        }

        private IngredientBl GetIngredient(string message, List<IngredientBl> ingredients)
        {
            Console.WriteLine(message);
            _ioHelper.PrintIngredients(ingredients);
            int chosenIngredient = _ioHelper.GetUserInputInt("Enter ingredient's number: ");
            int ingredientId = ingredients.ElementAt(chosenIngredient - 1).Id;
            return _ingredientsService.FindIngredient(ingredientId);
        }

        private void AddSauceToPizza(PizzaBl pizza)
        {
            while (pizza.SauceId == 0)
            {
                try
                {
                    pizza.SauceId = GetSauce("Available sauces: ", _saucesService.GetAllSauces()).Id;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private SauceBl GetSauce(string message, List<SauceBl> sauces)
        {
            Console.WriteLine(message);
            _ioHelper.PrintSauces(sauces);
            int chosenSauce = _ioHelper.GetUserInputInt("Enter sauce's number: ");
            int sauceId = sauces.ElementAt(chosenSauce - 1).Id;
            return _saucesService.FindSauce(sauceId);
        }

        private void ShowMenu()
        {
            List<PizzaBl> pizzas = _pizzasService.ShowMenu();

            foreach (var pizza in pizzas)
            {
                _ioHelper.PrintPizzaInfo(pizza);
            }
        }

        private void Order()
        {
            if (_databaseService.CheckIfPizzasNotEmpty())
            {
                OrderBl order = new OrderBl { Cost = 0 };
                string addAnotherPizza = "y";

                do
                {
                    AddPizzaToOrder(order, ref addAnotherPizza);

                } while (addAnotherPizza == "y");

                AddExtraSauce(order);
                AddClientToOrder(_ioHelper.GetUserInputStr("Type your phone number") ,order);
                _ordersService.AddOrder(order);
                Console.WriteLine($"Total price of the order: {order.Cost:C}");
            }
            else { Console.WriteLine("The list of pizzas is empty."); }
        }

        private void AddPizzaToOrder(OrderBl order, ref string addAnotherPizza)
        {
            try
            {
                PizzaBl pizzaBl = GetPizza("Available pizzas: ", _pizzasService.GetAllPizzas());
                PizzaToOrderBl ordered = _ordersService.GetPizzaToOrder(pizzaBl, _ioHelper.GetSize("Choose pizza's size: S, M, L"));
                GetFreeSauce(ordered);
                _ordersService.AddPizzaToOrder(ordered);
                order.PizzasToOrderId.Add(ordered.Id);
                order.Cost += ordered.Price;
                addAnotherPizza = _ioHelper.GetUserInputStr("Do you want to order another pizza? (y/n)").ToLower();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private PizzaBl GetPizza(string message, List<PizzaBl> pizzas)
        {
            Console.WriteLine(message);
            int index = 1;

            foreach (var pizza in pizzas)
            {
                Console.Write($"{index}. ");
                _ioHelper.PrintPizzaInfo(pizza);
                index++;
            }

            int chosenPizza = _ioHelper.GetUserInputInt("Enter pizza's number: "); ;
            return pizzas.ElementAt(chosenPizza - 1);
        }

        private void GetFreeSauce(PizzaToOrderBl pizza)
        {
            if (_ioHelper.GetUserInputStr("Do you want free sauce? (y/n)").ToLower() == "y")
            {
                pizza.FreeSaucesId.Add(GetSauce("Choose one free sauce: ", _saucesService.GetAllSauces()).Id);
                if (pizza.PizzaSize == PizzaToOrderBl.Size.L)
                {
                    if (_ioHelper.GetUserInputStr("You can choose another free sauce. (y/n)").ToLower() == "y")
                    {
                        pizza.FreeSaucesId.Add(GetSauce("Choose sauce: ", _saucesService.GetAllSauces()).Id);
                    }
                }
            }
        }

        private void AddExtraSauce(OrderBl order)
        {
            string addAnotherSauce = _ioHelper.GetUserInputStr("Do you want to order an extra sauce? (y/n)").ToLower();

            while (addAnotherSauce == "y")
            {
                try
                {
                    SauceBl sauce = GetSauceWithPrice("Available sauces: ", _saucesService.GetAllSauces());
                    order.ExtraSaucesId.Add(sauce.Id);
                    order.Cost += sauce.Price;
                    addAnotherSauce = _ioHelper.GetUserInputStr("Do you want to add an extra sauce? (y/n)").ToLower();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private SauceBl GetSauceWithPrice(string message, List<SauceBl> sauces)
        {
            Console.WriteLine(message);
            _ioHelper.PrintSaucesWithPrice(sauces);
            int chosenSauce = _ioHelper.GetUserInputInt("Enter sauce's number: ");
            return sauces.ElementAt(chosenSauce - 1);
        }

        private void AddClientToOrder(string phoneNumber, OrderBl order)
        {
            var client = _clientsService.FindClient(phoneNumber);

            if (client != null)
            {
                order.ClientId = client.Id;
                _clientsService.UpdateClient(client.Id, DateTime.Now);
                return;
            }

            AddNewClient(phoneNumber, order);
        }

        private void AddNewClient(string phoneNumber, OrderBl order)
        {
            var client = new ClientBl
            {
                FirstName = _ioHelper.GetUserInputStr("Type your first name"),
                LastName = _ioHelper.GetUserInputStr("Type your last name"),
                Address = _ioHelper.GetUserInputStr("Type your address"),
                PhoneNumber = phoneNumber,
                Email = _ioHelper.GetUserInputStr("Type your email"),
                LastOrderDate = DateTime.Now
            };

            _clientsService.AddClient(client);
            order.ClientId = client.Id;
        }

        private void Exit()
        {
            Environment.Exit(0);
        }
    }

}
