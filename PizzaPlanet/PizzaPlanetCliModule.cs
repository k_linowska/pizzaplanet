﻿using Ninject.Modules;
using PizzaPlanet.Cli.Helpers;

namespace PizzaPlanet
{
    public class PizzaPlanetCliModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
        }
    }
}
