﻿using PizzaPlanet.Business.Models;
using PizzaPlanet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaPlanet.Cli.Helpers
{
    internal interface IIoHelper
    {
        string GetIngredientsForPizza(PizzaBl pizza);
        PizzaToOrderBl.Size GetSize(string message);
        decimal GetUserInputDecimal(string message);
        int GetUserInputInt(string message);
        string GetUserInputStr(string message);
        void PrintIngredients(List<IngredientBl> ingredients);
        void PrintPizzaInfo(PizzaBl pizza);
        string PrintPizzasPrices(PizzaBl pizza);
        void PrintSauces(List<SauceBl> sauces);
        void PrintSaucesWithPrice(List<SauceBl> sauces);
    }

    internal class IoHelper : IIoHelper
    {
        IIngredientsService _ingredientsService;
        ISaucesService _saucesService;

        public IoHelper(IIngredientsService ingredientsService, ISaucesService saucesService)
        {
            _ingredientsService = ingredientsService;
            _saucesService = saucesService;
        }

        public string GetUserInputStr(string message)
        {
            string result = "";

            do
            {
                Console.WriteLine(message);
                result = Console.ReadLine();
            } while (String.IsNullOrWhiteSpace(result));

            return result;
        }

        public decimal GetUserInputDecimal(string message)
        {
            decimal result;

            while (!decimal.TryParse(GetUserInputStr(message), out result))
            {
                Console.WriteLine("Input was not a number. Try again...");
            }

            return result;
        }

        public int GetUserInputInt(string message)
        {
            int result;

            while (!int.TryParse(GetUserInputStr(message), out result))
            {
                Console.WriteLine("Wrong input. Try again...");
            }

            return result;
        }

        public void PrintPizzaInfo(PizzaBl pizza)
        {
            Console.WriteLine($"{pizza.Name}. Price: {PrintPizzasPrices(pizza)}. " +
                                $"Ingredients: {GetIngredientsForPizza(pizza)} " +
                                $"Sauce: {_saucesService.FindSauce(pizza.SauceId).Name}.");
        }

        public string GetIngredientsForPizza(PizzaBl pizza)
        {
            string ingredients = "";

            if (pizza.Ingredients.Count == 0)
            {
                ingredients = string.Join(", ", pizza.IngredientsId.Select(i => _ingredientsService.FindIngredient(i).Name));
                ingredients += ".";
                return ingredients;
            }

            ingredients = string.Join(", ", pizza.Ingredients.Select(i => i.Name));
            ingredients += ".";
            return ingredients;
        }

        public string PrintPizzasPrices(PizzaBl pizza)
        {
            return $"Small - {pizza.PriceForSizeS:C}, Medium - {pizza.PriceForSizeM:C}, Large - {pizza.PriceForSizeL:C}";
        }

        public void PrintIngredients(List<IngredientBl> ingredients)
        {
            for (int i = 0; i < ingredients.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + ingredients.ElementAt(i).Name);
            }
        }

        public void PrintSauces(List<SauceBl> sauces)
        {
            for (int i = 0; i < sauces.Count; i++)
            {
                Console.WriteLine($"{i + 1}: {sauces.ElementAt(i).Name}");
            }
        }

        public void PrintSaucesWithPrice(List<SauceBl> sauces)
        {
            for (int i = 0; i < sauces.Count; i++)
            {
                Console.WriteLine($"{i + 1}: {sauces.ElementAt(i).Name}. Price: {sauces.ElementAt(i).Price:C}.");
            }
        }

        public PizzaToOrderBl.Size GetSize(string message)
        {
            PizzaToOrderBl.Size result;

            while (!Enum.TryParse(GetUserInputStr(message), out result))
            {
                Console.WriteLine("Wrong input");
            }

            return result;
        }
    }

}
