﻿using System;
using System.Collections.Generic;

namespace PizzaPlanet.Cli.Helpers
{
    internal interface IMenu
    {
        void Execute(string commandId);
        void PrintAllCommands();
        void SetCommand(Action command, string commandId);
    }

    internal class Menu : IMenu
    {
        private Dictionary<string, Action> _commands = new Dictionary<string, Action>();

        public void SetCommand(Action command, string commandId)
        {
            if (_commands.ContainsKey(commandId))
            {
                throw new Exception($"Command with id {commandId} already exists!");
            }

            _commands.Add(commandId, command);
        }

        public void Execute(string commandId)
        {
            if (!_commands.ContainsKey(commandId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var chosenCommand = _commands[commandId];
            chosenCommand();
        }

        public void PrintAllCommands()
        {
            Console.WriteLine("Please select an option:");
            int index = 1;

            foreach (var item in _commands)
            {
                Console.WriteLine($"{index}: {item.Key}");
                index++;
            }
        }
    }
}
