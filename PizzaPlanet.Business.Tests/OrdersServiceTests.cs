﻿using Moq;
using NUnit.Framework;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Business.Services;
using PizzaPlanet.Data;
using System;
using System.ComponentModel;

namespace PizzaPlanet.Business.Tests
{
    [TestFixture]
    public class OrdersServiceTests
    {
        [Test]
        public void GetPizzaToOrder_ValidInput_ValidOutput()
        {
            var pizza = new PizzaBl { Id = 1, PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mockTime = new Mock<IDateTimeProvider>();
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mockTime.Object, mockContext.Object);
            var pizzaToReturn = new PizzaToOrderBl
            {
                Pizza = pizza,
                PizzaSize = pizzaSize,
                Price = ordersService.GetPrice(pizzaSize, pizza),
                PizzaId = pizza.Id
            };

            var result = ordersService.GetPizzaToOrder(pizza, pizzaSize);

            Assert.AreEqual(result.Pizza, pizzaToReturn.Pizza);
            Assert.AreEqual(result.PizzaSize, pizzaToReturn.PizzaSize);
            Assert.AreEqual(result.Price, pizzaToReturn.Price);
            Assert.AreEqual(result.PizzaId, pizzaToReturn.PizzaId);
        }

        [Test]
        public void GetPizzaToOrder_InvalidInputPizzaObjectSetAsNull_NullReferenceExceptionIsThrown()
        {
            PizzaBl pizza = null;
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mockTime = new Mock<IDateTimeProvider>();
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mockTime.Object, mockContext.Object);

            try
            {
                var pizzaToReturn = new PizzaToOrderBl
                {
                    Pizza = pizza,
                    PizzaSize = pizzaSize,
                    Price = ordersService.GetPrice(pizzaSize, pizza),
                    PizzaId = pizza.Id
                };

                var result = ordersService.GetPizzaToOrder(pizza, pizzaSize);
            }
            catch (NullReferenceException e)
            {
                Assert.AreEqual("Object reference not set to an instance of an object.", e.Message);
            }
        }

        [Test]
        public void GetPizzaToOrder_InvalidInputWrongPizzaSizeEnumValue_InvalidEnumArgumentExceptionIsThrown()
        {
            PizzaBl pizza = new PizzaBl { Id = 1, PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 }; ;
            PizzaToOrderBl.Size pizzaSize = 0;
            var mockTime = new Mock<IDateTimeProvider>();
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mockTime.Object, mockContext.Object);

            try
            {
                var pizzaToReturn = new PizzaToOrderBl
                {
                    Pizza = pizza,
                    PizzaSize = pizzaSize,
                    Price = ordersService.GetPrice(pizzaSize, pizza),
                    PizzaId = pizza.Id
                };

                var result = ordersService.GetPizzaToOrder(pizza, pizzaSize);
            }
            catch (InvalidEnumArgumentException e)
            {
                Assert.AreEqual("Invalid pizza size enumeration value", e.Message);
            }
        }

        [Test]
        public void GetPrice_ValidInputPizzaSizeLNoDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Monday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(20, result);
        }

        [Test]
        public void GetPrice_ValidInputPizzaSizeMNoDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Monday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.M;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(15, result);
        }

        [Test]
        public void GetPrice_ValidInputPizzaSizeSNoDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Monday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.S;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void GetPrice_ValidInputWeekendDiscountOptionSunday_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Sunday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void GetPrice_ValidInputWeekendDiscountOptionSaturday_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Saturday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.S;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(5, result);
        }

        [Test]
        public void GetPrice_InputCountsForWednesdayDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(15, result);
        }

        [Test]
        public void GetPrice_InputDoesNotCountForWednesdayDiscountPizzaSizeS_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.S;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void GetPrice_InputDoesNotCountForWednesdayDiscountPizzaSizeM_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.M;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.GetPrice(pizzaSize, pizza);

            Assert.AreEqual(15, result);
        }

        [Test]
        public void GetPrice_InvalidInputPizzaObjectSetAsNull_NullReferenceExceptionIsThrown()
        {
            PizzaBl pizza = null;
            var pizzaSize = PizzaToOrderBl.Size.L;
            var mock = new Mock<IDateTimeProvider>();
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            try
            {
                var result = ordersService.GetPrice(pizzaSize, pizza);
            }
            catch (NullReferenceException e)
            {
                Assert.AreEqual("Object reference not set to an instance of an object.", e.Message);
            }
        }

        [Test]
        public void GetPrice_InvalidInputWrongPizzaSizeEnumValue_InvalidEnumArgumentExceptionIsThrown()
        {
            PizzaBl pizza = new PizzaBl { Id = 1, PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 }; ;
            PizzaToOrderBl.Size pizzaSize = 0;
            var mock = new Mock<IDateTimeProvider>();
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            try
            {
                var result = ordersService.GetPrice(pizzaSize, pizza);
            }
            catch (InvalidEnumArgumentException e)
            {
                Assert.AreEqual("Invalid pizza size enumeration value", e.Message);
            }
        }

        [Test]
        public void CheckDiscount_InputCountsForWednesdayDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            decimal price = 20;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.CheckDiscount(pizzaSize, pizza, price);

            Assert.AreEqual(15, result);
        }

        [Test]
        public void CheckDiscount_InputCountsForWeekendDiscountOptionSaturday_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Saturday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            decimal price = 20;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.CheckDiscount(pizzaSize, pizza, price);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void CheckDiscount_InputCountsForWeekendDiscountOptionSunday_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Sunday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.L;
            decimal price = 20;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.CheckDiscount(pizzaSize, pizza, price);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void CheckDiscount_InputDoesNotCountForWednesdayDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.S;
            decimal price = 10;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.CheckDiscount(pizzaSize, pizza, price);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void CheckDiscount_ValidInputNoDiscount_ValidOutput()
        {
            var dayOfWeek = DayOfWeek.Tuesday;
            var pizza = new PizzaBl { PriceForSizeS = 10, PriceForSizeM = 15, PriceForSizeL = 20 };
            var pizzaSize = PizzaToOrderBl.Size.S;
            decimal price = 10;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            var result = ordersService.CheckDiscount(pizzaSize, pizza, price);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void CheckDiscount_InvalidInputPizzaObjectSetAsNullWednesdayDiscount_NullReferenceExceptionIsThrown()
        {
            var dayOfWeek = DayOfWeek.Wednesday;
            PizzaBl pizza = null;
            var pizzaSize = PizzaToOrderBl.Size.L;
            decimal price = 10;
            var mock = new Mock<IDateTimeProvider>();
            mock.Setup(x => x.GetDayOfWeek()).Returns(dayOfWeek);
            var mockContext = new Mock<Func<IPizzaPlanetContext>>();
            var ordersService = new OrdersService(mock.Object, mockContext.Object);

            try
            {
                var result = ordersService.CheckDiscount(pizzaSize, pizza, price);
            }
            catch (NullReferenceException e)
            {
                Assert.AreEqual("Object reference not set to an instance of an object.", e.Message);
            }
        }
    }
}
